#!/bin/python3
# #+PROPERTY: header-args :tangle yes :tangle "~/Projects/csharp-tutor/uilib.py" :comments org :shebang "#!/bin/python3"


from picotui.widgets import *
from picotui.menu import *
from picotui.context import Context

#Menus like these will probably be common later on so why not make them into functions.

def main_menu(itemList, completionList):
    with Context():
        padding_h = 6
        padding_v = 6

        spacing_h = 16 # horizontal spacing between WListBox and WLabel
        spacing_v = 3 # vertical spacing between WListBox and WButton

        # get longest module name' length
        list_width = 0
        for module in itemList:
            if len(module) > list_width:
                list_width = len(module)
        list_height = 6 # maximum number of modules displayed at a time


        label_width = len("Complete: false") # 'false' is longer than 'true', but both options are possible

        width = padding_h + list_width + spacing_h + label_width
        height = padding_v + list_height + spacing_v + 1# label will only be one line, can be ignored; 1 is the height of the button

        startx = round((os.get_terminal_size()[0] / 2) - (width / 2))
        starty = round((os.get_terminal_size()[1] / 2) - (height / 2))

        d = Dialog(startx, starty, width, height)

        modules = WListBox(list_width, list_height, itemList)
        d.add(padding_h, padding_v, modules)

        label_text= "Complete: false"
        label = WLabel(label_text)
        d.add(width - (label_width + padding_h), round((height / 2) - 1), label)

        btn_dialog = "OK"
        btn_width = len(btn_dialog) + 4
        btn = WButton(btn_width , btn_dialog)
        d.add(round((width / 2) - (btn_width / 2)), round(padding_v / 2 + list_height + spacing_v), btn)
        btn.finish_dialog = btn_dialog

        redraw_screen(d, Screen)
        Screen.set_screen_redraw(redraw_screen)

        m = WMenuBar([])
        m.permanent = True
        m.redraw()

        returnSelection = True
        return loop(m, d, returnSelection) # res

def non_interactive_page(text):
    with Context():
        text = text.split('\n')
        
        padding_h = 6
        padding_v = 4

        spacing_v = 1 # vertical spacing between WListBox and WButton

        # get the number of lines within the 'text' string by counting the number of \n's in it
        text_height = len(text);
        text_width = 0
        for line in text:
            if len(line) > text_width:
                text_width = len(line)

        width = padding_h + text_width
        height = padding_v + text_height + spacing_v + 1# label will only be one line, can be ignored; 1 is the height of the button

        startx = round((os.get_terminal_size()[0] / 2) - (width / 2))
        starty = round((os.get_terminal_size()[1] / 2) - (height / 2))

        d = Dialog(startx, starty, width, height)

        if len(text) == 1:
            d.add(round((width / 2) - (text_width / 2)), round(padding_v / 3), WLabel(text[0]))
        else:
            i = 0
            for line in text:
                d.add(round((width / 2) - (len(line) / 2)), round(padding_v / 3) + i, WLabel(text[i]))
                i = i + 1
        
        btn_dialog = "Next"
        btn_width = len(btn_dialog) + 4
        btn = WButton(btn_width , btn_dialog)
        d.add(round((width / 2) - (btn_width / 2)), round(padding_v / 2 + text_height + spacing_v), btn)
        btn.finish_dialog = btn_dialog

        redraw_screen(d, Screen)
        Screen.set_screen_redraw(redraw_screen)

        m = WMenuBar([])
        m.permanent = True
        m.redraw()

        returnSelection = True
        return loop(m, d, returnSelection) # res
    
def two_choice(title, opt1 = "Yes", opt2 = "No"):
    with Context():
        title = title.split('\n')

        padding_h = 6
        padding_v = 6 # includes vertical space between title and buttons

        btn_spacing = 2 # space between the two buttons
        btn1_width = len(opt1) + 4
        btn2_width = len(opt2) + 4
        buttons_combined_width = (btn1_width + btn2_width + btn_spacing)

        title_length = 0
        for line in title:
            if len(line) > title_length:
                title_length = len(line)
            else:
                continue


        width = None
        if title_length > buttons_combined_width:
            width = title_length + padding_h
        else:
            width = buttons_combined_width + padding_h
        height = len(title) + padding_v

        startx = round((os.get_terminal_size()[0] / 2) - (width / 2))
        starty = round((os.get_terminal_size()[1] / 2) - (height / 2))

        ACTION_ONE = opt1
        ACTION_TWO = opt2

        d = Dialog(startx, starty, width, height)

        if len(title) == 1:
            d.add(round((width / 2) - (title_length / 2)), round(padding_v / 3), WLabel(title[0]))
        else:
            i = 0
            for line in title:
                d.add(round((width / 2) - (len(line) / 2)), round(padding_v / 3) + i, WLabel(title[i]))
                i = i + 1

        btn1 = WButton(btn1_width , opt1)
        d.add(round((width / 2) - (btn1_width + (btn_spacing / 2))), (round(padding_v * (2 / padding_v)) + len(title)), btn1)
        btn1.finish_dialog = ACTION_ONE

        btn2 = WButton(btn2_width , opt2)
        d.add(round((width / 2) + (btn_spacing / 2)), (round(padding_v * (2 / padding_v)) + len(title)), btn2)
        btn2.finish_dialog = ACTION_TWO

        redraw_screen(d, Screen)
        Screen.set_screen_redraw(redraw_screen)

        m = WMenuBar([])
        m.permanent = True
        m.redraw()

        return loop(m, d) # res

def input(title, button_text = "OK"):
    with Context():
        title = title.split('\n')
        input_min_width = 20

        padding_h = 6
        padding_v = 7 # includes vertical space between title, input and buttons

        title_length = 0
        for line in title:
            if len(line) > title_length:
                title_length = len(line)
            else:
                continue

        width = title_length + padding_h
        if width - padding_h < input_min_width: # title too short for comfortable input length
            width = input_min_width + padding_h
        height = len(title) + padding_v

        startx = round((os.get_terminal_size()[0] / 2) - (width / 2))
        starty = round((os.get_terminal_size()[1] / 2) - (height / 2))

        d = Dialog(startx, starty, width, height)

        if len(title) == 1:
            d.add(round((width / 2) - (title_length / 2)), round(padding_v / 3), WLabel(title[0]))
        else:
            i = 0
            for line in title:
                d.add(round((width / 2) - (len(line) / 2)), round(padding_v / 3) + i, WLabel(title[i]))
                i = i + 1

        input_width = width - padding_h
        input = WTextEntry(input_width, "type here")
        d.add(round((width / 2) - (input_width / 2)), round(padding_v * (2 / padding_v) + len(title)), input)

        btn_width = len(button_text) + 4
        btn = WButton(btn_width, button_text)
        d.add(round((width / 2) - (btn_width / 2)), (round(padding_v * (3 / padding_v) + len(title) + 1)), btn) # that +1 being the text entry
        btn.finish_dialog = button_text

        redraw_screen(d, Screen)
        Screen.set_screen_redraw(redraw_screen)

        m = WMenuBar([])
        m.permanent = True
        m.redraw()

        if loop(m, d):
            return input.get()

#Main loop for dialogs

def loop(m, d, returnSelection = False):
    while True:
        key = m.get_input()

        if isinstance(key, list):
            # Probably a mouse click idk
            x, y = key
            if m.inside(x, y):
                m.focus = True
            elif d.inside(x,y):
                d.focus = True

        if m.focus:
            res = m.handle_input(key)

            if res is not None and res is not True:
                return res
        else:
            # if out of focus, menu can be focused by pressing F9
            if key == KEY_F9:
                m.focus = True
                m.redraw()
                continue

            #otherwise, handle input
            res = d.handle_input(key)
            if res is not None and res is not True:
                if returnSelection is True:
                    for child in d.childs:
                        if type(child) == WListBox:
                            return child.get_cur_line()
                else:
                    return res

def redraw_screen(d, s, allow_cursor=False):
    s.attr_color(C_MAGENTA, C_BLACK)
    s.cls()
    s.attr_reset()
    d.redraw()


#A little friendly touch. Confirm whether $EDITOR environment variable exists and whether the executable saved in it is really the user's preferred editor.

def set_editor():
    import subprocess
    output = str(subprocess.check_output('echo $EDITOR', shell=True).decode('utf-8'))
    if output != "\n": # editor env. variable exists
        if two_choice("It looks like your preferred\ntext editor is set to %s\nIs this alright?\n(You can change it later)\n" % output, "Sure", "Nah") == "Sure":
            return output
        else:
            return input("Please specify your\npreferred text editor by\nit's name or absolute path:\n")
    else:
        return input("Please specify your\npreferred text editor by\nit's name or absolute path:\n")
