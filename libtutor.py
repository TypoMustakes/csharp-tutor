#!/bin/python3
# Module

# A module holds multiple lessons that fall under a common theme. For example, lessons about the ~while~ loop, ~for~ loop, the ~if-else~ statement, the ~switch~ statement and the ~do-while~ loop would be a part of a module called *Conditionals and Loops*


class Module:
	def __init__(self, name, lessons, complete):
		self.Name = name
		self.Lessons = lessons #array
		self.Complete = complete

# Lesson

# Behold the Lesson class. Its only purpose is to hold pages and separate them into digestible, modular parts. It does not contain any specific text itself. That I will probably store in its own text file or something, idk. But don't peek into that file to check the correct answers, you filthy cheater.

# ...

# You know what? I might just hash it. That's right, that's what you deserve for even thinking you could cheat, you punk. Gonna cry? Gonna piss your pants maybe?


class Lesson:
    def __init__(self, name, pages, complete):
        self.Name = name
        self.Pages = pages #array
        self.Complete = complete

# Page

# A page contains the text, instructions, buttons, input and basically all content of a lesson. A lesson may have as many pages as necessary, but it will only be complete once all pages have been completed within in.

# Pages may contain the following:
# - Title
# - Text
# - Quizes


class Page:
    def __init__(self, title, index, text, quiz):
        self.Title = title
        self.Index = index
        self.Text = text
        self.Quiz = quiz

# Quiz

# A quiz is a part of a page that proposes some sort of task to the user, then checks whether the submitted answer was correct or not. This task may be a true-or-false question, or a multiple selection type thing. Tasks that include complex editing, like filling out missing parts of code, or just writing code in general, will be handled differently, and will be unrelated to this class.

# The ~allow_multiple~ field describes if the user should be able to select multiple options, or only a single one. Boolean type variable.


class Quiz:
    def __init__(self, title, options, allow_multiple, solution):
        self.Title = title
        self.Options = options
        self.Allow_Multiple = allow_multiple
        self.Solution = solution #hash

# Get module names

# Get a list of all modules' names, and only their names


def get_module_names():
    from globalvars import MODULES
    list = []
    for item in MODULES:
        list.append(item.Name)

    return list

# Get longest module name

# Calculate the longest module name, for the purpose of creating UI boxes of appropriate sizes.


def get_longest_module_name():
    max = 0
    for module in get_module_names():
        if len(module) > max:
            max = len(module)

    return max

# Get module completion list

# Get a list of boolean values representing the completion state of each module.


def get_module_completion_list():
      import globalvars
      list = []
      for module in globalvars.MODULES:
            list.append(module.Complete)

      return list

# Get lesson names

# Get a list of lessons' names associated to a module, and only their names. The parameter "module" should be a Module object to which the desired lessons belong to


def get_lesson_names(module):
    list = []
    for x in module.Lessons:
        list.append(x.Name)

    return list

# Get lesson completion list

# Get a boolean list with each value representing the completion state of a given modules's lessons. Parameter should be a module object


def get_lesson_completion_list(module):
    list = []
    for lesson in module.Lessons:
        list.append(lesson.Complete)

    return list

# Get module object by name

# This method relies on the fact that there should never be two modules with the same name inside the ~res/course.json~ file, so please keep it that way, kind stranger


def get_module_by_name(moduleName):
    import globalvars
    for module in globalvars.MODULES:
        if module.Name == moduleName:
            return module

# Get lesson object by name

# Same as the one above, but for lessons. Same naming policy applies.


def get_lesson_by_name(lessonName):
    import globalvars
    for module in globalvars.MODULES:
        for lesson in module.Lessons:
            if lesson.Name == lessonName:
                return lesson

# Get page titles

# Get a list of all pages' titles inside a given lesson


def get_page_names(lesson):
    list = []
    for page in lesson.Pages:
        list.append(page.Title)

    return list

# Page interactivity check

# Checks if a page is interactive or not. This is achieved by checking if the page contains a "quiz" object or not. Returns a boolean value.


def is_page_interactive(page):
    if 'quiz' in locals():
        return True
    else:
        return False

# Get first incomplete page from a lesson


def get_first_page(lesson):
   # for page in lesson.Pages:
   #     if page.Complete is False:
   #         return page
    return lesson.Pages[0] #DEBUG
