#!/bin/python3
# Profile

# This used to be an object, but due to the nature of this program, only one profile should be loaded at one time, so creating an object would be misleading in the future.

# This +object+ file contains every piece of data contained within your ~profile.json~ file, as interpreted by ~libjson.py~. See ~libjson.org --> dump/read_profile()~ for more details.

# Parameters may be added later as needed. +I myself haven't yet figured out how I'll store a profile's course progress so for now I'll just store the preferred text editor's path.+

# The progress of the course will be contained within the ~res/course.json~ file, as dictated by the numerous ~complete~ variables. For this to work, we need to change these boolean values as the user completes parts of the course, then write the modified course data to disk.

# In order not to overwrite the 'default' state of the course (as in nothing is flagged as 'complete' yet), I will only read the ~course.json~ file if a user-modified copy of it doesn't exist. If it does, the ~Course_Path~ string will point the program to the user-modified version of the file.

# "But why not just create a method that changes all the 'complete' flags to 'false'?"
# Great question! Because in case a new profile is created while another one already exists (and has made progress in their course), it's easier to make a copy of an existing file than to copy someone else's and *then* change all flags to 'false'.


Name = ""
Editor = ""
Course_Path = ""
Profile_Path = ""

def create()
    Name = uilib.input("It seems like no user profile exists.\nI'll create one for you.\nWhat's your name?")

    #Then we set the editor. See uilib.py
    Editor = uilib.set_editor()

    #create new copy of res/course.json with the profile's folder
    Course_Path = './res/profiles/'+Name+'/course.json'
    from shutil import copyfile
    copyfile('./res/course.json', Course_Path)

    Profile_Path = './res/profiles/'+Name+'/profile.json'
