#!/bin/python3
# #+PROPERTY: header-args :tangle yes :tangle "~/Projects/csharp-tutor/libjson.py" :comments org :shebang "#!/bin/python3"


import json
from libtutor import *
# importing profile shouldn't be neccesary, as this module is called by libtutor which is called by csharptutor, which has already imported profile

# Init

# This piece of code serves to interpret ~<project-source>/res/course.json~ and create the appropriate objects based on its contents. The idea is that if I decide to add something new to the course, I can just edit only the json file, and the program will interpret these changes at runtime.

# This field (~JSON_DATA~) will globally hold the data stored within ~res/course.json~ after ~json_init()~ runs.


PATH = "./res/course.json"
FILE = open(PATH)
JSON_DATA = json.load(FILE)
FILE.close()

# Module object creation

# Generate an array of Modules. This will include every single module inside the ~res/course.json~ file, including all of their lessons and all pages of all lessons as well.


def generate_modules():
    list = []
    for x in JSON_DATA["modules"]:
        name = x["moduleName"]
        lessons = generate_lessons(name)
        complete = x["complete"]

        module = Module(name, lessons, complete)
        list.append(module)

    return list

# Lesson object creation

# Generate Lesson objects from JSON_DATA belonging to a given module.


def generate_lessons(moduleName):
    list = []
    for x in JSON_DATA["modules"]:
        if moduleName == x["moduleName"]:
            for y in x["lessons"]:
                name = y["lessonName"]
                pages = generate_pages(moduleName, name)
                complete = y["complete"]

                lesson = Lesson(name, pages, complete)
                list.append(lesson)

    return list

# Page object creation

# Generate Page objects from JSON_DATA belonging to a given lesson.


def generate_pages(moduleName, lessonName):
    list = []
    for x in JSON_DATA["modules"]:
        if moduleName == x["moduleName"]:
            for y in x["lessons"]:
                if lessonName == y["lessonName"]:
                    for z in y["pages"]:
                        title = z["title"]
                        index = z["pageIndex"]
                        text = z["text"]
                        quiz = "" #temporarily

                        page = Page(title, index, text, quiz);
                        list.append(page)

    return list

# Profile

# This function dumps the contents of ~profile.py~ to ~<project-source>/res/profiles/<profile-name>/profile.json~. This includes everything like preferred text editor path, nickname, etc. Basically it's your save file.


def dump_profile():
    FILE = open(profile.Profile_Path, "w")
    data = json.dumps(profile.__dict__)
    FILE.writelines(data)
    FILE.close()



# Now we do it the reverse way and read the json file and upload its contents to the ~profile~ module.
# //TODO


def read_profile(name):
    FILE = open("./res/profiles/"+name+"/profile.json", "r")
    PROFILE_DATA = json.load(FILE)

    profile.Name = PROFILE_DATA["Name"]
    profile.Editor = PROFILE_DATA["Editor"]
    profile.Course_Path = PROFILE_DATA["Course_Path"]
    profile.Profile_Path = PROFILE_DATA["Profile_Path"]
