#!/bin/python3
# #+PROPERTY: header-args :tangle yes :tangle "~/Projects/csharp-tutor/csharptutor.py" :comments org :shebang "#!/bin/python3"


import uilib
import os
import subprocess
import libtutor #uses libjson.py when needed, no need to import
import profile

# Main
# First of all, we must check if the dotnet core SDK is installed and if not, then exit.

# +For this, we will use the ~whereis~ binary, as it's a part of the ~util-linux~ (or similar, name may differ) package on most GNU/Linux systems.+
# No dumbass just try to run ~dotnet --version~ what the fuck.

# We ignore the version number for now, altough it would be possible to either use the host system's package manager to check for the latest version, or we could fetch it from dotnet.microsoft.com.


try:
    subprocess.check_output('dotnet --version', shell=True)
except:
    print("\nAttempting to run 'dotnet --version' on your system returned the error above.\nThis might mean that the .NET SDK is not installed, or it's broken somehow, please check the error message.")

    exit()



# Then we load a chosen profile.


if subprocess.check_output('ls res/profiles/', shell=True) == "\n": #no user profile exists
    profile.create()
else #at least one user exists, bring up the menu



# #+end_src


# Spawn the main menu from which the user may select modules to complete. That will return a ~Module~ object. Using that, we will spawn a ~Lesson~ object the user chooses.


while True:
    MODULE = libtutor.get_module_by_name(
                      uilib.main_menu(libtutor.get_module_names(),
                                      libtutor.get_module_completion_list()))

    LESSON = libtutor.get_lesson_by_name(
                      uilib.main_menu(libtutor.get_lesson_names(MODULE),
                                      libtutor.get_lesson_completion_list(MODULE)))

    VALUE = uilib.non_interactive_page(libtutor.get_first_page(LESSON).Text)
  #
  #  uilib.page(libjson.get_pages(JSON_DATA, MODULE, LESSON)) #TODO: to determine which page to display (by index), we must first find a way to save and load a profile's progression and check which page the user is currently on inside a given module's lesson
