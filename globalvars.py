#!/bin/python3
# #+PROPERTY: header-args :tangle yes :tangle "~/Projects/csharp-tutor/globalvars.py" :comments org :shebang "#!/bin/python3"
 
# We now tell ~libjson~ to fill the ~MODULES~ variable with actual data, assisted by ~libtutor~. We need this to avoid a circular import problem.


import libjson
MODULES = libjson.generate_modules()
